#include "mpi.h"
#include <stdio.h>
#include <cmath>

const int N = 1000;

int main(int argc, char **argv) {
  int me, size;
  const int tag = 700;
  const int master = 0;

  MPI_Status status;
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &me);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  int* v = new int[N];
  for (int i = 0; i < N; i++) v[i] = i;

  const int start_at = floor(N * ((double)me / size));
  const int end_at = floor(N * (((double)me + 1) / size));

  int part_sum = 0;
  for (auto x = (v + start_at); x < (v + end_at); x++) part_sum += *x;

  if (me == master) {
    int sum = part_sum;
    for (int slave = 1; slave < size; slave++) {
      int fragment;
      MPI_Recv (&fragment, 1, MPI_INT, slave, tag, MPI_COMM_WORLD, &status);
      sum += fragment;
    }
    printf("Master: sum is %d\n", sum);
  }

  if (me != master) {
    MPI_Send(&part_sum, 1, MPI_INT, master, tag, MPI_COMM_WORLD);
  }

  delete v;

  printf("Node #%d: fragment is %d\n", me, part_sum);

  MPI_Finalize();

  return 0;
}
