#include "mpi.h"
#include <stdio.h>
#include <cmath>
#include <cstdlib>

int main(int argc, char **argv) {
  int me, size;
  const int tag = 700;
  const int master = 0;

  MPI_Status status;
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &me);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  const long long N = atoll(std::getenv("N"));

  double* v = new double[N];
  for (int i = 0; i < N; i++) v[i] = 1;

  const double start_moment = MPI_Wtime();

  const int start_at = floor(N * ((double)me / size));
  const int end_at = floor(N * (((double)me + 1) / size));

  double part_sum = 0;
  for (int i = start_at; i < end_at; i++) part_sum += v[i];


  if (me == master) {
    double sum = part_sum;
    for (int slave = 1; slave < size; slave++) {
      double fragment;
      MPI_Recv (&fragment, 1, MPI_DOUBLE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      sum += fragment;
    }

    const double end_moment = MPI_Wtime();

    printf("Master: took %fs to compute partial sum.\n", end_moment - start_moment);
    printf("Master: sum is %f\n", sum);
  }

  if (me != master) {
    MPI_Send(&part_sum, 1, MPI_DOUBLE, master, tag, MPI_COMM_WORLD);
  }

  delete v;
  MPI_Finalize();
  return 0;
}
