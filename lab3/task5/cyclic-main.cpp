#include "mpi.h"
#include <stdio.h>
#include <cmath>

const int N = 10000000;

int main(int argc, char **argv) {
  int me, size;
  const int tag = 700;
  const int master = 0;

  MPI_Status status;
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &me);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  double* v = new double[N];
  for (int i = 0; i < N; i++) v[i] = 1;

  const int start_at = floor(N * ((double)me / size));
  const int end_at = floor(N * (((double)me + 1) / size));

  const double start_moment = MPI_Wtime();

  double part_sum = 0;
  for (int i = me; i < N; i+= size) part_sum += v[i];

  const double end_moment = MPI_Wtime();

  printf("Proc %d: took %fs to compute local sum.\n", me, end_moment - start_moment);

  if (me == master) {
    double sum = part_sum;
    for (int slave = 1; slave < size; slave++) {
      double fragment;
      MPI_Recv (&fragment, 1, MPI_DOUBLE, slave, tag, MPI_COMM_WORLD, &status);
      sum += fragment;
    }
    printf("Master: sum is %f\n", sum);
  } else {
    MPI_Send(&part_sum, 1, MPI_DOUBLE, master, tag, MPI_COMM_WORLD);
  }

  delete v;

  printf("Proc #%d: local is %f\n", me, part_sum);

  MPI_Finalize();

  return 0;
}
