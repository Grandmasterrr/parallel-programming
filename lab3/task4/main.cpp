#include "mpi.h"
#include <stdio.h>
#include <cmath>

const int N = 100 * 1000 * 1000;

int main(int argc, char **argv) {
  int rank, size;
  const int tag = 700;
  const int master = 0;

  MPI_Status status;
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  int* v = new int[N];
  for (int i = 0; i < N; i++) v[i] = 1;


  const int start_at = floor(N * ((double)rank / size));
  const int end_at = floor(N * (((double)rank + 1) / size));

  const double start_moment = MPI_Wtime();

  long part_sum = 0;
  for (int i = rank; i < N; i+= size) part_sum += v[i];

  const double end_moment = MPI_Wtime();

  printf("Node %d took %fs to compute local sum\n", rank, end_moment - start_moment);

  if (rank == master) {
    long sum = part_sum;
    for (int slave = 1; slave < size; slave++) {
      long fragment;
      MPI_Recv (&fragment, 1, MPI_LONG, slave, tag, MPI_COMM_WORLD, &status);
      sum += fragment;
    }
    printf("Master: sum is %ld\n", sum);
  }

  if (rank != master) {
    MPI_Send(&part_sum, 1, MPI_LONG, master, tag, MPI_COMM_WORLD);
  }

  delete v;

  printf("Node #%d: fragment is %ld\n", rank, part_sum);

  MPI_Finalize();
  return 0;
}
