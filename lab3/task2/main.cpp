#include "mpi.h"
#include <stdio.h>

int main(int argc, char **argv) {
  int me, size;
  int tag = 700;

  MPI_Status status;
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &me);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  const bool i_am_sender = me % 2 == 0;
  const bool i_am_receiver = !i_am_sender;

  if (i_am_sender) {
    const int target = me + 1;
    const bool recepient_available = target < size;

    if (recepient_available) {
      const auto message = me*me + 100;
      MPI_Send (&message, 1, MPI_INT, target, tag, MPI_COMM_WORLD);
      printf("Node #%d send \"%d\" to #%d\n", me, message, target);
    }
  }

  if (i_am_receiver) {
    const int source = me - 1;
    const bool source_available = source >= 0;

    if (source_available) {
      int inbox;
      MPI_Recv (&inbox, 1, MPI_INT, source, tag, MPI_COMM_WORLD, &status);
      printf("Node #%d received \"%d\" from #%d\n", me, inbox, source);
    }
  }

  MPI_Finalize();
  return 0;
}
