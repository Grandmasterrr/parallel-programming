#include <stdio.h>
#include <mpi.h>
#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char** argv) {
  int size, rank;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  const int N = atoi(std::getenv("N"));

  double* v = new double[N];
  for (int i = 0; i < N; i++) v[i] = i;

  long sum = 0;
  for (auto x = (v + rank); x < (v + N); x += size) sum += *x;

  printf("Node #%d of %d said: %d\n", rank, size, (int)sum);

  delete v;
  MPI_Finalize();
  return 0;
}
