#include <stdio.h>
#include <mpi.h>

int size, rank;

int main(int argc, char** argv) {
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  printf ("Hello, I’m node #%d from %d\n", rank, size);

  MPI_Finalize();

  return 0;
}
