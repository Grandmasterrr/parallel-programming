#include <stdio.h>
#include <mpi.h>
#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char** argv) {
  int size, rank;

  // Инициализировать MPI
  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  const int N = atoi(std::getenv("N"));

  double* v = new double[N];

  for (int i = 0; i < N; i++) v[i] = i;

  // Почитать начало и конец расчета
  const int start = floor(N * ((double)rank / size));
  const int end = floor(N * (((double)rank + 1) / size));

  // Посчитать сумму
  double sum = 0;
  for (auto x = (v + start); x < (v + end); x++) sum += *x;

  // Вывести сумму в stdout
  printf("Node #%d of %d partial sum: %d\n", rank, size, (int)sum);

  delete v;

  MPI_Finalize();

  return 0;
}
