#include <stdio.h>
#include <omp.h>
#include <cstdlib>
#include <chrono>
#include <cmath>

int main() {
  const int N = atoi(std::getenv("N"));
  const int T = atoi(std::getenv("T"));

  double * const a = new double[N];
  double * const b = new double[N];

  for (int i = 0; i < N; i++) {
    a[i] = 1;
    b[i] = 1;
  }

  double s = 0;

  const double startAt = omp_get_wtime();

  #pragma omp parallel  num_threads(T)
  {
    const int threads = omp_get_num_threads();
    printf("%d threads\n", threads);
  }

  #pragma omp reduction (+ : s) num_threads(T)
  for (int i = 0; i < N; i++)
    s += a[i] * b[i] + sin(i) - sin(i) + cos(i) - cos(i);

  const double endAt = omp_get_wtime();

  printf("Sum is %f\n", s);
  printf("Took %f s\n", endAt - startAt);

  delete a;
  delete b;

  return 0;
}
