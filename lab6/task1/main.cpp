#include <stdio.h>
#include <omp.h>
#include <cstdlib>
#include <chrono>

int main() {
  const int N = atoi(std::getenv("N"));

  int * const v = new int[N];
  for (int* x = v; x < v + N; x++)
    (*x) = rand() % 10;

  long sum = 0;

  const auto start = std::chrono::system_clock::now();

  #pragma omp parallel for shared(sum)
  for (int i = 0; i < N; i++)
    sum += v[i];

  const auto end = std::chrono::system_clock::now();

  printf("Sum is %ld\n", sum);
  printf("Took %lld nanosec\n", static_cast<long long int>((end - start).count()));

  delete v;
  return 0;
}
