#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

const int T =  atoi(std::getenv("T"));

double ** createMatrix(const int rows, const int cols) {
  double ** const m = new double * [rows];

  for (int i = 0; i < rows; i++)
    m[i] = new double[cols];

  return m;
}

void fillMatrix(double ** m, int rows, int cols, double v) {
  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      m[i][j] = v;
}

double ** multipleMatrices(double ** m1, double ** m2, int N, int M, int L) {
  double ** const m3 = createMatrix(N, L);
  fillMatrix(m3, N, L, 0);

  #pragma omp parallel for schedule(dynamic) num_threads(T)
  for (int i = 0; i < N; i++)
    for (int j = 0; j < L; j++)
      for (int u = 0; u < M; u++)
        m3[i][j] += m1[i][u] + m2[u][j];

  return m3;
}

void deleteMatrix(double ** const m, const int rows) {
  for (int i = 0; i < rows; i++)
    delete m[i];

  delete m;
}

int main() {
  const int N = atoi(std::getenv("N"));
  const int M = atoi(std::getenv("M"));
  const int L = atoi(std::getenv("L"));

  double ** const m1 = createMatrix(N, M);
  double ** const m2 = createMatrix(M, L);

  fillMatrix(m1, N, M, 1);
  fillMatrix(m2, N, M, 1);

  const double start = omp_get_wtime();
  double ** const m3 = multipleMatrices(m1, m2, N, M, L);
  const double end = omp_get_wtime();

  printf("Took %f s to multiple matrices\n", end - start);

  deleteMatrix(m1, N);
  deleteMatrix(m2, M);
  deleteMatrix(m3, N);
}
