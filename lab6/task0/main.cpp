#include <stdio.h>
#include <omp.h>
#include <cstdlib>

int main() {
  int tid;
  int last = -1;

  #pragma omp parallel private(tid) shared(last)
  {
    tid = omp_get_thread_num();

    while (last != tid - 1) {
      continue;
    }

    printf("I am %d\n", tid);
    last = tid;
  }

  return 0;
}
