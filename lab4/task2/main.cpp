#include "mpi.h"
#include <stdio.h>
#include <cmath>


int main(int argc, char **argv) {
  const int tag = 700;
  const int master = 0;
  const int N = 1000000;

  int me, size;

  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &me);
  MPI_Comm_size (MPI_COMM_WORLD, &size);

  long* v = new long[N];

  for (int i = 0; i < N; i++) v[i] = i;

  long part_sum = 0;
  for (int i = me; i < N; i += size) part_sum += v[i];

  printf("Node #%d partial sum: %ld\n", me, part_sum);

  if (me != master) {
    MPI_Request req;
    MPI_Isend (&part_sum, 1, MPI_LONG, master, tag, MPI_COMM_WORLD, &req);
  }

  if (me == master) {
    const short int slaves = size - 1;

    MPI_Status* stats = new MPI_Status[slaves];
    MPI_Request* reqs = new MPI_Request[slaves];
    long* fragments = new long[slaves];

    for (int slave = 0; slave < slaves; slave++) {
      const int slave_rank = slave + 1;
      MPI_Irecv(fragments + slave, 1, MPI_LONG, slave_rank, tag, MPI_COMM_WORLD, reqs + slave);
    }

    MPI_Waitall(slaves, reqs, stats);

    long sum = part_sum;
    for (int i = 0; i < slaves; i++) sum += fragments[i];

    printf("Master: sum is %ld\n", sum);

    delete reqs;
    delete stats;
    delete fragments;
  }

  delete v;
  MPI_Finalize();
  return 0;
}
