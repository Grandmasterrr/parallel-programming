#include "mpi.h"
#include <stdio.h>
#include <cmath>

const auto context = MPI_COMM_WORLD;
const int tag = 700;
const int master = 0;
const int N = 100;

int main(int argc, char **argv)
{
  int me, size;

  MPI_Init (&argc, &argv);
  MPI_Comm_rank (context, &me);
  MPI_Comm_size (context, &size);

  long* v = new long[N];

  const auto start_moment = MPI_Wtime();

  for (int i = 0; i < N; i++) v[i] = i;

  long part_sum = 0;
  for (int i = me; i < N; i += size) part_sum += v[i];

  printf("Node #%d partial sum: %ld\n", me, part_sum);

  if (me != master)
  {
    MPI_Request req;
    MPI_Isend (&part_sum, 1, MPI_LONG, master, tag, context, &req);
  }

  if (me == master)
  {
    const int slaves = size - 1;

    MPI_Status* stats = new MPI_Status[slaves];
    MPI_Request* reqs = new MPI_Request[slaves];
    long* fragments = new long[slaves];

    for (int slave = 0; slave < slaves; slave++)
    {
      const int slave_rank = slave + 1;
      int flag;
      do {
        MPI_Iprobe(slave_rank, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, MPI_STATUS_IGNORE);
      } while (!flag);
      MPI_Irecv(fragments + slave, 1, MPI_LONG, slave_rank, tag, context, reqs + slave);
    }

    const auto end_moment = MPI_Wtime();

    long sum = part_sum;
    for (int i = 0; i < slaves; i++) sum += fragments[i];

    printf("Master: sum is %ld\n", sum);
    printf("Master: took %fs to gather results\n", end_moment - start_moment);

    delete reqs;
    delete stats;
    delete fragments;
  }

  delete v;
  MPI_Finalize();
  return 0;
}
