#include "mpi.h"
#include <stdio.h>

int main(int argc, char **argv) {
  int nodes, rank, inbox[2], tag1 = 1, tag2 = 2;

  MPI_Request reqs[4];
  MPI_Status stats[4];
  MPI_Init(&argc,&argv);

  MPI_Comm_size(MPI_COMM_WORLD, &nodes);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int prev = rank - 1;
  int next = rank + 1;

  if (rank == 0) prev = nodes - 1;
  if (rank == (nodes - 1)) next = 0;

  MPI_Irecv(&inbox[0], 1, MPI_INT, prev, tag1, MPI_COMM_WORLD, &reqs[0]);
  MPI_Irecv(&inbox[1], 1, MPI_INT, next, tag2, MPI_COMM_WORLD, &reqs[1]);
  MPI_Isend(&rank, 1, MPI_INT, prev, tag2, MPI_COMM_WORLD, &reqs[2]);
  MPI_Isend(&rank, 1, MPI_INT, next, tag1, MPI_COMM_WORLD, &reqs[3]);

  MPI_Waitall(4, reqs, stats);

  printf("Node #%d send \"%d\" to #%d\n", rank, rank, prev);
  printf("Node #%d send \"%d\" to #%d\n", rank, rank, next);
  printf("Node #%d received \"%d\" from #%d\n", rank, inbox[0], prev);
  printf("Node #%d received \"%d\" from #%d\n", rank, inbox[1], next);

  MPI_Finalize();
  return 0;
}
