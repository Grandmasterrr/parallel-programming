#include <mpi.h>
#include <stdio.h>
#include <cmath>
#include <cstdlib>
#include <iostream>

const int MASTER = 0;

void work_as_master(const int &size) {
  // Определить N. N только у мастера, ок?
  int N;
  std::cin >> N;

  // Создать вектор
  double * const v = new double[N];
  for (int i = 0; i < N; i++)
    v[i] = 1;

  // Определить и разослать размеры локальных векторов
  int * const local_sizes = new int[size];
  for (int i = 0; i < size; i++)
    local_sizes[i] =  floor((double)(i + 1) * N / size) - floor((double)i * N / size);
  MPI_Scatter(local_sizes, 1, MPI_INT, 0, 0, MPI_INT, MASTER, MPI_COMM_WORLD);

  // Определить где начинаются куски локальных векторов. Разослать куски.
  double * const local_v = new double[local_sizes[MASTER]];
  int * const placements = new int[size];
  placements[0] = 0;
  for (int i = 1; i < size; i++)
    placements[i] = placements[i - 1] + local_sizes[i - 1];

  MPI_Scatterv(v, local_sizes, placements, MPI_DOUBLE, local_v, local_sizes[MASTER], MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

  // Посчитать свою локальную сумму
  double local_sum = 0;
  for (int i = 0; i < local_sizes[MASTER]; i++)
    local_sum += local_v[i];

  // Поучаствовать в Reduce и забрать полную сумму
  double full_sum;
  MPI_Reduce(&local_sum, &full_sum, 1, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);
  printf("Master: full sum is %f\n", full_sum);

  delete v;
  delete local_sizes;
  delete local_v;
  delete placements;
}

void work_as_slave() {
  // Поучаствовать в Scatter и получить количество элементов в куске
  int local_items_count;
  MPI_Scatter(0, 0, MPI_INT, &local_items_count, 1, MPI_INT, MASTER, MPI_COMM_WORLD);

  // Поучаствовать в Scatter и получить свой кусок вектора
  double * const v = new double[local_items_count];
  MPI_Scatterv(0, 0, 0, MPI_DOUBLE, v, local_items_count, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);

  // Поучаствовать в Reduce, поместив туда свою локальную сумму
  double local_sum = 0;
  for (int i = 0; i < local_items_count; i++) local_sum += v[i];
  MPI_Reduce(&local_sum, 0, 1, MPI_DOUBLE, MPI_SUM, MASTER, MPI_COMM_WORLD);

  delete v;
}

int main(int argc, char** argv) {
  int rank, size;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  if (rank == MASTER) work_as_master(size);
  else work_as_slave();

  MPI_Finalize();
  return 0;
}
