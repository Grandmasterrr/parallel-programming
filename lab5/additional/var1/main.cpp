#include <mpi.h>
#include <stdio.h>
#include <cmath>
#include <cstdlib>

int main(int argc, char** argv) {
  const int master = 0;
  int rank, size;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  const int N = atoi(std::getenv("N"));

  const int local_items_count = floor(((double)rank + 1) * N / size) - floor((double)rank * N / size);

  double * const v_local = new double[local_items_count];

  if (rank == master) {
    double * const v = new double[N];
    int * const local_sizes = new int[size];
    int * const local_placements = new int[size];

    for (int i = 0; i < N; i++)
      v[i] = 1;

    for (int i = 0; i < size; i++)
      local_sizes[i] = floor(((double)i + 1) * N / size) - floor((double)i * N / size);

    for (int i = 0; i < size; i++)
      local_placements[i] = floor((double)i * N / size);

    MPI_Scatterv (
      v,
      local_sizes,
      local_placements,
      MPI_DOUBLE,
      v_local,
      local_items_count,
      MPI_DOUBLE,
      master,
      MPI_COMM_WORLD
    );

    delete v;
    delete local_sizes;
    delete local_placements;
  }

  if (rank != master)
    MPI_Scatterv(0, 0, 0, MPI_DOUBLE, v_local, local_items_count, MPI_DOUBLE, master, MPI_COMM_WORLD);

  double local_sum = 0;
  for (int i = 0; i < local_items_count; i++)
    local_sum += v_local[i];

  double full_sum;
  MPI_Reduce(&local_sum, &full_sum, 1, MPI_DOUBLE, MPI_SUM, master, MPI_COMM_WORLD);

  if (rank == master)
    printf("Master: Vector sum is %f\n", full_sum);

  delete v_local;
  MPI_Finalize();
  return 0;
}
