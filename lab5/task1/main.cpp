#include "mpi.h"
#include <stdio.h>
#define SIZE 4

float** initMatrix(const int rows, const int &cols) {
  float** m = new float*[rows];
  for (auto row = m; row != m + rows; row++) *row = new float[cols];
  return m;
}

void fillMatrix(float** m, const int &rows, const int &cols) {
  for (int row = 0; row < rows; row++)
    for (int col = 0; col < cols; col++)
      m[row][col] = rows * cols - 2 * rows + cols;
}

void deleteMatrix(float** m, const int &rows) {
  for (auto row = m; row != m + rows; row++) delete (*row);
  delete m;
}

int main(int argc, char **argv) {
  int numtasks, rank, sendcount, recvcount, source;
  float sendbuf[SIZE][SIZE] = {
    {1.0, 2.0, 3.0, 4.0},
    {5.0, 6.0, 7.0, 8.0},
    {9.0, 10.0, 11.0, 12.0},
    {13.0, 14.0, 15.0, 16.0}
  };
  float recvbuf[SIZE];

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

  if (numtasks == SIZE) {
    source = 1;
    sendcount = SIZE;
    recvcount = SIZE;
    MPI_Scatter(sendbuf, sendcount, MPI_FLOAT, recvbuf, recvcount, MPI_FLOAT, source, MPI_COMM_WORLD);
    printf("Node #%d. Results: %f %f %f %f\n", rank, recvbuf[0], recvbuf[1], recvbuf[2], recvbuf[3]);
  } else {
    printf("Число процессов должно быть равно %d. \n", SIZE);
  }

  MPI_Finalize();
  return 0;
}
