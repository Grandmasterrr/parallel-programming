#include "mpi.h"
#include <stdio.h>
#include <cmath>

const int N = 1000;

int main(int argc, char **argv) {
  int me, size;
  const int tag = 700;
  const int master = 0;

  MPI_Status status;
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &me);
  MPI_Comm_size (MPI_COMM_WORLD, &size);


  int* v = new int[N];

  for (int i = 0; i < N; i++) v[i] = i;

  // Посчитать частичную сумму
  const int start = floor(N * ((double)me / size));
  const int end = floor(N * (((double)me + 1) / size));
  int part_sum = 0; for (auto x = (v + start); x < (v + end); x++) part_sum += *x;


  int* fragments = new int[size];

  const auto start_moment = MPI_Wtime();
  MPI_Gather(&part_sum, 1, MPI_INT, fragments, 1, MPI_INT, master, MPI_COMM_WORLD);


  if (me == master) {
    int full_sum = 0; for (auto x = (fragments + 0); x < (fragments + size); x++) full_sum += *x;

    const auto end_moment = MPI_Wtime();

    printf("Master: answer is %d\n", full_sum);
    printf("Master: took %fs to accumulate results\n", end_moment - start_moment);
  }

  printf("Node #%d said: %d\n", me, part_sum);

  delete v;
  delete fragments;

  MPI_Finalize();
  return 0;
}
