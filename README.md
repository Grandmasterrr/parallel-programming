# Лабы по параллельному программированию

# Требования

- g++
- MPICH
- OpenMP (для лаб 6 и далее)

# Сборка

```

mpicxx main.cpp -o main.out

```

# Запуск

```

mpiexec -n 4 ./main.out

```
